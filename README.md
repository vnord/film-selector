A simple utility for managing which films to watch during movie nights. A file containing the films is required (in the future it will be possible to create one from scratching using the utility). The utility will spit out a random film from the file, and fetch poster+metadata from OMDB.

Source file format (excluding the '{' and '}' characters):

{Director's name} - {Optional: title in original language {AKA}} {IMDB-compatible title} {[year]}

Example:
Farhadi, Asghar - Jodaeiye Nader az Simin AKA A Separation [2011]

A blank line is required before the section containg films that have already been watched. If there are none, a blank line is still necessary at the end of the file.
