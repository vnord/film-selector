package filmselector;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class Main extends Application {

	final Button rollButton = new Button("Roll!");
	final Button openButton = new Button("Select text file");
	final Button updateButton = new Button("Mark as watched");

	final FileChooser fileChooser = new FileChooser();
	
	URL defaultImageURL = Main.class.getResource("/film.jpg");


	Image defaultPoster = new Image(defaultImageURL.toExternalForm(), 175.0, 250.0, false, false);



	static GridPane infoGrid = new GridPane();
	static GridPane infoGridInner = new GridPane();
	


	Image poster = defaultPoster;


	ImageView posterView = new ImageView();


	@Override
	public void start(final Stage stage) throws Exception {

		stage.setTitle("Ari's FilmSelector v0.4.4");
		
		stage.getIcons().add(defaultPoster);


		labelSetup(infoGridInner, infoGrid);


		openButton.setOnAction(
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(final ActionEvent e) {
						FilmSelector.configureFileChooser(fileChooser);
						FilmSelector.file = fileChooser.showOpenDialog(stage);
						if (FilmSelector.file != null) {
							try {
								FilmSelector.readFile(FilmSelector.file);
								rollButton.setDisable(false);
							} catch (FileNotFoundException e1) {
								e1.printStackTrace();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
						}
					}
				});


		rollButton.setOnAction(
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(final ActionEvent e) {
						try {
							FilmSelector.getNextFilm();
							poster = new Image(FilmSelector.film.getPosterUrl(), 175.0, 250.0, false, false);
							posterView.setImage(poster);
//							File defaultFile = new File(System.getProperty("user.dir") + "/src/film.jpg");
//							Image defaultPoster = new Image(defaultFile.toURI().toString(), 175.0, 250.0, false, false);
							infoGridPopulate(FilmSelector.film, infoGridInner);	
						} catch (Exception e1) {
							poster = defaultPoster;
						}
						updateButton.setDisable(false);
					}
				});

		rollButton.setDisable(true);

		updateButton.setOnAction(
				new EventHandler<ActionEvent>() {
					@Override
					public void handle(final ActionEvent e) {

						Alert alert = new Alert(AlertType.CONFIRMATION);
						alert.setTitle("Confirm overwrite");
						alert.setHeaderText("Are you sure you want to update the file?");

						Optional<ButtonType> result = alert.showAndWait();
						if (result.get() == ButtonType.OK){
							FilmSelector.addWatchedFilm(FilmSelector.selectedFilm);
							FilmSelector.removeUnwatchedFilm(FilmSelector.selectedFilm);
							try {
								FilmSelector.updateFile();
							} catch (FileNotFoundException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							} catch (IOException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							updateButton.setDisable(true);
						}
					}
				});

		updateButton.setDisable(true);


		posterView.setImage(poster);





		final GridPane inputGridPane = new GridPane();
		GridPane.setConstraints(openButton, 0, 0);
		GridPane.setConstraints(rollButton, 1, 0);
		GridPane.setConstraints(updateButton, 0,4);
		GridPane.setConstraints(posterView, 0,2);
		GridPane.setConstraints(infoGrid, 1, 2);


		inputGridPane.setVgap(6);
		inputGridPane.setHgap(6);
		inputGridPane.getChildren().addAll(openButton, rollButton,
				updateButton, posterView, infoGrid);

		final Pane rootGroup = new VBox(12);

		rootGroup.getChildren().addAll(inputGridPane);

		FindMore.findMoreSetup();

		rootGroup.setPadding(new Insets(12, 12, 12, 12));

		
		Tab existingTab = new Tab("From file", rootGroup);
		Tab findNewTab = new Tab("Find more", FindMore.rootGroup);

		existingTab.setClosable(false);
		findNewTab.setClosable(false);


		TabPane tabs = new TabPane(existingTab, findNewTab);




		stage.setScene(new Scene(tabs, 780, 390));
		stage.show();
	}

	public static void labelSetup(GridPane inner, GridPane outer){

		GridPane labelGrid = new GridPane();

		//   infoGrid.setGridLinesVisible(true);

		for(int i = 0; i < 11; i++){
			Text tmp = new Text(0,0, "N/A");
			GridPane.setConstraints(tmp,2,i);
			inner.getChildren().add(tmp);
		}

		GridPane.setConstraints(inner, 1,0);



		Text titleLabel = new Text (10,50, "Title");
		Text actorsLabel = new Text (10,50, "Actors");
		Text directorLabel = new Text (10,50, "Director");
		Text runtimeLabel = new Text (10,50, "Length");
		Text genreLabel = new Text (10,50, "Genre");
		Text plotLabel = new Text (10,50, "Plot");
		Text writerLabel = new Text (10,50, "Writer");
		Text languageLabel = new Text (10,50, "Language");
		Text yearLabel = new Text (10, 50, "Year");
		Text imdbLabel = new Text (10, 50, "IMDB");
		Text rtLabel = new Text (10, 50, "RT");

		GridPane.setConstraints(titleLabel, 1, 0);
		GridPane.setConstraints(directorLabel, 1, 1);
		GridPane.setConstraints(writerLabel, 1, 2);
		GridPane.setConstraints(actorsLabel, 1, 3);
		GridPane.setConstraints(genreLabel, 1, 4);
		GridPane.setConstraints(yearLabel, 1, 5);
		GridPane.setConstraints(languageLabel, 1, 6);
		GridPane.setConstraints(runtimeLabel, 1, 7);


		GridPane.setConstraints(imdbLabel, 1, 8);
		GridPane.setConstraints(rtLabel, 1, 9);

		GridPane.setConstraints(plotLabel, 1, 10);


		labelGrid.getChildren().addAll(titleLabel, directorLabel, actorsLabel,
				runtimeLabel, genreLabel, plotLabel, writerLabel, languageLabel,
				imdbLabel, rtLabel, yearLabel);


		outer.setVgap(60);
		outer.setHgap(15);
		
		outer.getChildren().addAll(labelGrid, inner);


	}

	public static void infoGridPopulate(Film f, GridPane gp){

		gp.getChildren().clear();

		String writerFixed;

		if(f.getWriter().length() > 70)
			writerFixed = (f.getWriter().substring(0, f.getWriter().indexOf(',')) + " et al");
		else
			writerFixed = f.getWriter();

		String actorsFixed;

		if(f.getActors().length() > 70)
			actorsFixed = (f.getActors().substring(0, f.getActors().indexOf(',')) + " et al");
		else
			actorsFixed = f.getActors();


		Text title = new Text(0, 0, f.getTitle());
		Text actors = new Text(10, 50, actorsFixed);
		Text director = new Text(10, 50, f.getDirector());
		Text genre = new Text(10, 50, f.getGenre());
		Text language = new Text(10, 50, f.getLanguage());
		Text shortPlot = new Text(10, 50, f.getShortPlot());
		Text writer = new Text(10, 50, writerFixed);
		Text year = new Text(10, 50, f.getYear());
		Text imdb = new Text(10, 50, f.getImdb());
		Text rt = new Text(10, 50, f.getRt());


		shortPlot.setWrappingWidth(500);

		writer.maxWidth(500);



		int hours = f.getRuntime() / 60;
		int minutes = f.getRuntime() % 60;

		String minuteString = minutes < 10 ? "0" + Integer.toString(minutes) : Integer.toString(minutes);

		Text runtime = new Text(10, 50, Integer.toString(hours) + ":" + minuteString);

		GridPane.setConstraints(title, 1, 0);
		GridPane.setConstraints(director, 1, 1);
		GridPane.setConstraints(writer, 1, 2);
		GridPane.setConstraints(actors, 1, 3);
		GridPane.setConstraints(shortPlot, 1, 10);
		GridPane.setConstraints(year, 1, 5);
		GridPane.setConstraints(language, 1, 6);
		GridPane.setConstraints(runtime, 1, 7);
		GridPane.setConstraints(genre, 1, 4);
		GridPane.setConstraints(imdb, 1, 8);
		GridPane.setConstraints(rt, 1, 9);

		gp.getChildren().addAll(title,actors,director,genre,language,shortPlot,
				rt, imdb, writer,year,runtime);


		GridPane.setConstraints(gp, 1, 0);		

	}


	public static void main(String[] args) {
		Application.launch(args);
	}

}
