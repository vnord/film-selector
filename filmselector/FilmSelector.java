package filmselector;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import javafx.stage.FileChooser;

public class FilmSelector {

	static BufferedReader br;
	static String everything;
	static String useful;
	static ArrayList<String> allWatchedFilms;
	static ArrayList<String> allUnwatchedFilms;
	static String unwatched;
	static ArrayList<String> watched;
	static int filmPos;
	static String selectedFilm;
	static File file;
	
	public static Film film;
	

	static void configureFileChooser(final FileChooser fileChooser){                           
		fileChooser.setTitle("Open Text File");
		fileChooser.setInitialDirectory(
				new File(System.getProperty("user.home"))
				); 
	}

	public static void readFile(File fileName) throws IOException, FileNotFoundException {
		BufferedReader br = new BufferedReader(new FileReader(fileName));
		try {
			StringBuilder sb = new StringBuilder();
			String line = br.readLine();

			while (line != null) {
				sb.append(line);
				sb.append(System.lineSeparator());
				line = br.readLine();
			}

			everything = sb.toString();
		} finally {
			br.close();
		}

	}


	public static void handleDataShit(){
		String[] fullString = everything.split("\\n\\n");
		String unwatchedString = fullString[0];
		String watchedString = fullString[1];
		allWatchedFilms = new ArrayList<String>(Arrays.asList(watchedString.split("\\n")));
		allUnwatchedFilms = new ArrayList<String>(Arrays.asList(unwatchedString.split("\\n")));
	}


	public static void updateFile() throws FileNotFoundException, IOException{
		try {
			BufferedWriter outputWriter = new BufferedWriter(new FileWriter(file));
			for (int i = 0; i < allUnwatchedFilms.size(); i++){
				outputWriter.write(allUnwatchedFilms.get(i));
				outputWriter.newLine();
			}
			outputWriter.write("\n");
			for (int i = 0; i < allWatchedFilms.size(); i++){
				outputWriter.write(allWatchedFilms.get(i));
				outputWriter.newLine();
			}
			outputWriter.flush();
			outputWriter.close();
		} catch (IOException e) {}
		
		readFile(file);
	}
	
	public static void addWatchedFilm(String f){
		allWatchedFilms.add(selectedFilm);
	}
	
	public static void removeUnwatchedFilm(String f){
		allUnwatchedFilms.remove(selectedFilm);
	}
	
	public static void addUnwatchedFilm(String f){
		allUnwatchedFilms.add(f);
	}

	public static String getNextFilm() throws Exception{

		handleDataShit();


		Random rand = new Random();
		int n = rand.nextInt(allUnwatchedFilms.size());

		selectedFilm = allUnwatchedFilms.get(n);


		while (selectedFilm.charAt(selectedFilm.length() - 1) == '*'){
			n--;
			selectedFilm = allUnwatchedFilms.get(n);
		};

		filmPos = n;
		
		film = new Film(selectedFilm);
		

		return selectedFilm;
	}

}