package filmselector;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Film {

	private String year;

	private int runtime;

	private String title;
	private String director;

	private String language;

	private String genre;

	private String shortPlot;
	private String longPlot;


	private String actors;
	private String writer;

	private String awards;
	private JSONArray ratingsArray;

	private String imdb;
	private String rt;

	private String posterUrl;



	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public String getShortPlot() {
		return shortPlot;
	}

	public void setShortPlot(String shortPlot) {
		this.shortPlot = shortPlot;
	}

	public String getLongPlot() {
		return longPlot;
	}

	public void setLongPlot(String longPlot) {
		this.longPlot = longPlot;
	}

	public String getGenre() {
		return genre;
	}

	public void setGenre(String genre) {
		this.genre = genre;
	}

	public String getActors() {
		return actors;
	}

	public void setActors(String actors) {
		this.actors = actors;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getAwards() {
		return awards;
	}

	public void setAwards(String awards) {
		this.awards = awards;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getPosterUrl() {
		return posterUrl;
	}

	public void setPosterUrl(String posterUrl) {
		this.posterUrl = posterUrl;
	}

	public int getRuntime() {
		return runtime;
	}

	public void setRuntime(int runtime) {
		this.runtime = runtime;
	}

	public void setRuntime(String runtime) {
		this.runtime = Integer.parseInt(runtime);
	}

	public Film (String title, String year) {
		this.title = title;
		this.year = year;
	}

	public Film (String title, Boolean b) {
		this.title = title;
	}

	public Film(String raw) throws Exception{
		if (raw.contains("AKA")) {
			this.title = raw.substring(raw.indexOf("AKA") + 4, raw.indexOf('[') - 1);
		} 
		else
		{
			this.title = raw.substring(raw.indexOf('-') + 2, raw.indexOf('[') - 1);
		}

		this.year = raw.substring(raw.indexOf('[') + 1, raw.indexOf(']'));

		selfPopulate(true);
	}

	public void selfPopulate(Boolean hasYear) throws Exception{

		JSONObject filmJSON;

		if (hasYear) {
			filmJSON = InfoFetch.getByTitleAndYear(title, year);

		} else {
			filmJSON = InfoFetch.getByTitle(title);
			try {
				year = filmJSON.getString("Year");
			} finally {}
		}
		
		try {
			title = filmJSON.getString("Title");
		} finally {}
		
		try {
			director = filmJSON.getString("Director");
		} finally {};

		try {
			language = filmJSON.getString("Language");
		} finally {};

		try {
			genre = filmJSON.getString("Genre");
		} finally {};

		try {
			shortPlot = filmJSON.getString("Plot");
		} finally {};

		try {
			actors = filmJSON.getString("Actors");
		} finally {};

		try {
			writer = filmJSON.getString("Writer");
		} finally {};

		try {
			String rtTmp = filmJSON.getString("Runtime");			
			runtime = Integer.parseInt(rtTmp.substring(0, rtTmp.indexOf(' ')));
		} finally {};

		try {
			ratingsArray = filmJSON.getJSONArray("Ratings");
			JSONObject imdbRatingJSON = ratingsArray.getJSONObject(0);
			imdb = imdbRatingJSON.getString("Value");
		} catch(JSONException e) {imdb = "N/A";}
		finally {};

		try {
			ratingsArray = filmJSON.getJSONArray("Ratings");
			JSONObject rottenTomatoesJSON = ratingsArray.getJSONObject(1);
			rt = rottenTomatoesJSON.getString("Value");
		} catch(JSONException e) {rt = "N/A";}
		finally {};




		try {
			posterUrl = filmJSON.getString("Poster");
		} finally {};

	}

	public String getImdb() {
		return imdb;
	}

	public void setImdb(String imdb) {
		this.imdb = imdb;
	}

	public String getRt() {
		return rt;
	}

	public void setRt(String rt) {
		this.rt = rt;
	}


}
