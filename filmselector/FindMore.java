package filmselector;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;

public class FindMore {
	
	private static Film film;


	static File defaultFile = new File(System.getProperty("user.dir") + "/src/film.jpg");


	static GridPane infoGrid = new GridPane();
	static GridPane infoGridInner = new GridPane();
	
	static URL defaultImageUrl = Main.class.getResource("/film.jpg");

	static Image defaultPoster = new Image(defaultImageUrl.toExternalForm(), 175.0, 250.0, false, false);



	static ImageView posterView = new ImageView();

	static Image poster = defaultPoster;
	
	static Button addButton = new Button("Add to file");




	final GridPane inputGridPane = new GridPane();
	
	static TextField year = new TextField();
	static TextField title = new TextField();
	
	static  Text statusField = new Text("");





	public static Pane rootGroup = new VBox(12);
	
	public static void submitMethod(){
		if ((year.getText() != null && !year.getText().isEmpty())) {
			film = new Film(title.getText(), year.getText());
			try {
				statusField.setText("");
				film.selfPopulate(true);
				poster = new Image(film.getPosterUrl(), 175.0, 250.0, false, false);
				posterView.setImage(poster);
				Main.infoGridPopulate(film, infoGridInner);
				if(FilmSelector.file != null)
					addButton.setDisable(false);
			} catch (Exception e1) {
				statusField.setText("Film not found.");
			}

		} else if (title.getText() != null && !title.getText().isEmpty()) {
			film = new Film(title.getText(), true);
			try {
				statusField.setText("");
				film.selfPopulate(false);
				poster = new Image(film.getPosterUrl(), 175.0, 250.0, false, false);
				posterView.setImage(poster);
				Main.infoGridPopulate(film, infoGridInner);
				if(FilmSelector.file != null)
					addButton.setDisable(false);
			} catch (Exception e1) {
				statusField.setText("Film not found.");
			}

		}
	}





	public static void findMoreSetup(){

		posterView.setImage(poster);

		
		HBox topBox = new HBox();
	    topBox.setSpacing(5);
	    
	    statusField.autosize();
	    
	    statusField.setFill(Color.RED);



		final GridPane inputGridPane = new GridPane();
		GridPane.setConstraints(posterView, 0, 2);
		GridPane.setConstraints(infoGrid, 1, 2);


		title.setPromptText("Enter Film Title");
		title.setPrefColumnCount(10);
		title.getText();

		year.setPromptText("Year (opt)");
		year.setPrefColumnCount(5);
		year.getText();

		Button submit = new Button("Go!");
		
		addButton.setDisable(true);

		
		topBox.getChildren().addAll(title, year, submit, statusField);
		

		submit.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				submitMethod();
			}
		});
		
		title.setOnKeyPressed(new EventHandler<KeyEvent>()
	    {
	        @Override
	        public void handle(KeyEvent ke)
	        {
	            if (ke.getCode().equals(KeyCode.ENTER))
	            {
	                submitMethod();
	            	title.clear();
	            	year.clear();
	            }
	        }
	    });
		
		year.setOnKeyPressed(new EventHandler<KeyEvent>()
	    {
	        @Override
	        public void handle(KeyEvent ke)
	        {
	            if (ke.getCode().equals(KeyCode.ENTER))
	            {
	                submitMethod();
	            	title.clear();
	            	year.clear();
	            }
	        }
	    });
		
		addButton.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				
				String director = film.getDirector();
				String firstName = director.substring(0,director.indexOf(' '));
				String lastName = director.substring(director.indexOf(' ') + 1);
				
				String title = film.getTitle();
				
				String year = film.getYear();
				
				String fullString = (lastName + ", " + firstName + " - " + title + " [" + year + "]");
				
				FilmSelector.addUnwatchedFilm(fullString);
				
				try {
					FilmSelector.updateFile();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				addButton.setDisable(true);
				
				
			}
		
		});


		Main.labelSetup(infoGridInner, infoGrid);
		


		inputGridPane.setHgap(6);
		
		GridPane.setConstraints(addButton,0,4);
		GridPane.setConstraints(statusField,2,2);
		
		inputGridPane.getChildren().addAll(posterView, infoGrid);
		

		rootGroup = new VBox(12);

		rootGroup.getChildren().addAll(topBox, inputGridPane, addButton);
		
		rootGroup.setPadding(new Insets(12, 12, 12, 12));




	}

}
