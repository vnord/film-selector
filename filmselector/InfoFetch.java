package filmselector;

import java.net.URL;
import java.net.URLEncoder;
import java.util.Scanner;

import org.json.JSONObject;


public class InfoFetch {
	
	private static JSONObject obj;
	
	public static JSONObject getByTitle(String title) throws Exception
	{
	    // build a URL
	    String s = "http://www.omdbapi.com/?t=";
	    s += URLEncoder.encode(title, "UTF-8");
	    URL url = new URL(s);
	 
	    // read from the URL
	    Scanner scan = new Scanner(url.openStream());
	    String str = new String();
	    while (scan.hasNext())
	        str += scan.nextLine();
	    scan.close();
	 
	    // build a JSON object
	    obj = new JSONObject(str);
	    if (! obj.getString("Response").equals("True"))
	        return null;
	    
	    return obj;
	}
	
	public static JSONObject getByTitleAndYear(String title, String year) throws Exception
	{
	    // build a URL
	    String s = "http://www.omdbapi.com/?t=";
	    s += URLEncoder.encode(title, "UTF-8");
	    s += "&y=";
	    s += URLEncoder.encode(year,"UTF-8");
	    URL url = new URL(s);
	 
	    // read from the URL
	    Scanner scan = new Scanner(url.openStream());
	    String str = new String();
	    while (scan.hasNext())
	        str += scan.nextLine();
	    scan.close();
	 
	    // build a JSON object
	    obj = new JSONObject(str);
	    if (! obj.getString("Response").equals("True"))
	        return null;
	    
	    return obj;
	}
	
}